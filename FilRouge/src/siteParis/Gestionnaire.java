package siteParis;

public class Gestionnaire {
	//attributs de la classe
	private String passwordGestionnaire;
	
	//constructeur de la classe
	public Gestionnaire(String passwordGestionnaire) {
		this.passwordGestionnaire = passwordGestionnaire;

	}
	/**
	 * Getter of the property <tt>motDePasse</tt>
	 * 
	 * @return Returns the motDePasse.
	 * @uml.property name="motDePasse"
	 */
	public String getMotDePasse() {
		return passwordGestionnaire;
	}

	/**
	 * Setter of the property <tt>motDePasse</tt>
	 * 
	 * @param motDePasse The motDePasse to set.
	 * @uml.property name="motDePasse"
	 */
	public void setMotDePasse(String passwordGestionnaire) {
		this.passwordGestionnaire = passwordGestionnaire;
	}
    
	public void rightPassword(String passwordGestionnaire) throws MetierException {
		 if(!this.passwordGestionnaire.equals(passwordGestionnaire)) throw new MetierException();	
	}
	/**
	 */
	
}
