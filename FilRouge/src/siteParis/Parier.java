package siteParis;

/**
 * @author daniel
 *
 */
public class Parier {
	//attributs de la classe
	private String nomCompetition;
	private String vainqueurEnvisage;
	private long miseEnJetons;
	private boolean solde;
	
	//constructeur de la classe
	public Parier(String nomCompetition, String vainqueurEnvisage, long miseEnJetons) {		
		this.nomCompetition = nomCompetition;
		this.vainqueurEnvisage = vainqueurEnvisage;
		this.miseEnJetons = miseEnJetons;
		this.solde = false;
	}
	
	public boolean isSolde() {
		return solde;
	}
	public void setSolde(boolean solde) {
		this.solde = solde;
	}
	public String getNomCompetition() {
		return nomCompetition;
	}
	public void setNomCompetition(String nomCompetition) {
		this.nomCompetition = nomCompetition;
	}
	public String getVainqueurEnvisage() {
		return vainqueurEnvisage;
	}
	public void setVainqueurEnvisage(String vainqueurEnvisage) {
		this.vainqueurEnvisage = vainqueurEnvisage;
	}
	public long getMiseEnJetons() {
		return miseEnJetons;
	}
	public void setMiseEnJetons(long miseEnJetons) {
		this.miseEnJetons = miseEnJetons;
	}
	
	
}
