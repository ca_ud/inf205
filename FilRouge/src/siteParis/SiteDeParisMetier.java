package siteParis;

import java.util.LinkedList;

/**
 * 
 * @author Bernard Prou et Julien Mallet <br>
 *         <br>
 *         La classe qui contient toutes les méthodes "Métier" de la gestion du
 *         site de paris. <br>
 *         <br>
 *         Dans toutes les méthodes :
 *         <ul>
 *         <li>un paramètre de type <code>String</code> est invalide si il n'est
 *         pas instancié.</li>
 *         <li>pour la validité d'un password de gestionnaire et d'un password
 *         de joueur :
 *         <ul>
 *         <li>lettres et chiffres sont les seuls caractères autorisés</li>
 *         <li>il doit avoir une longueur d'au moins 8 caractères</li>
 *         </ul>
 *         </li>
 *         <li>pour la validité d'un pseudo de joueur :
 *         <ul>
 *         <li>lettres et chiffres sont les seuls caractères autorisés</li>
 *         <li>il doit avoir une longueur d'au moins 4 caractères</li>
 *         </ul>
 *         </li>
 *         <li>pour la validité d'un prénom de joueur et d'un nom de joueur :
 *         <ul>
 *         <li>lettres et tiret sont les seuls caractères autorisés</li>
 *         <li>il doit avoir une longueur d'au moins 1 caractère</li>
 *         </ul>
 *         </li>
 *         <li>pour la validité d'une compétition :
 *         <ul>
 *         <li>lettres, chiffres, point, trait d'union et souligné sont les
 *         seuls caractères autorisés</li>
 *         <li>elle doit avoir une longueur d'au moins 4 caractères</li>
 *         </ul>
 *         </li>
 *         <li>pour la validité d'un compétiteur :
 *         <ul>
 *         <li>lettres, chiffres, trait d'union et souligné sont les seuls
 *         caractères autorisés</li>
 *         <li>il doit avoir une longueur d'au moins 4 caractères.</li>
 *         </ul>
 *         </li>
 *         </ul>
 */

public class SiteDeParisMetier {

	private LinkedList<Joueur> joueurs;

	private LinkedList<Joueur> joueursRetire;
	private LinkedList<Competition> competitions;
	private LinkedList<Competition> competitionsSolde;

	private Gestionnaire gestionnaire;

	/**
	 * constructeur de <code>SiteDeParisMetier</code>.
	 * 
	 * @param passwordGestionnaire le password du gestionnaire.
	 * 
	 * @throws MetierException levée si le <code>passwordGestionnaire</code> est
	 *                         invalide
	 */
	public SiteDeParisMetier(String passwordGestionnaire) throws MetierException {
		
		//verifier format du password
		validitePasswordGestionnaire(passwordGestionnaire); 
		this.gestionnaire = new Gestionnaire(passwordGestionnaire);
		
		joueurs = new LinkedList<Joueur>();
		competitions = new LinkedList<Competition>();
		joueursRetire = new LinkedList<Joueur>();
		competitionsSolde = new LinkedList<Competition>();

	}

	//* Les méthodes du gestionnaire (avec mot de passe gestionnaire)

	/**
	 * inscrire un joueur.
	 * 
	 * @param nom                  le nom du joueur
	 * @param prenom               le prénom du joueur
	 * @param pseudo               le pseudo du joueur
	 * @param passwordGestionnaire le password du gestionnaire du site
	 * 
	 * @throws MetierException         levée si le <code>passwordGestionnaire</code>
	 *                                 proposé est invalide, si le
	 *                                 <code>passwordGestionnaire</code> est
	 *                                 incorrect.
	 * @throws JoueurExistantException levée si un joueur existe avec les mêmes noms
	 *                                 et prénoms ou le même pseudo.
	 * @throws JoueurException         levée si <code>nom</code>,
	 *                                 <code>prenom</code>, <code>pseudo</code> sont
	 *                                 invalides.
	 * 
	 * @return le mot de passe (déterminé par le site) du nouveau joueur inscrit.
	 */
	public String inscrireJoueur(String nom, String prenom, String pseudo, String passwordGestionnaire)
			throws MetierException, JoueurExistantException, JoueurException {
		
		validitePasswordGestionnaire(passwordGestionnaire);	  //MetierException
		this.gestionnaire.rightPassword(passwordGestionnaire);//MetierException
		validiteJoueur(nom, prenom, pseudo); 				  //JoueurException		
		validiteJoueurExistant(new Joueur(nom, prenom, pseudo));
		
		// pas d'erreur donc on ajoute le joueur 
		joueurs.add(new Joueur(nom, prenom, pseudo));
		String pwd = getJoueur(nom, prenom, pseudo).generatePassword();
		getJoueur(nom, prenom, pseudo).setPassword(pwd);
		return pwd;
	}

	/**
	 * supprimer un joueur.
	 * 
	 * @param nom                  le nom du joueur
	 * @param prenom               le prénom du joueur
	 * @param pseudo               le pseudo du joueur
	 * @param passwordGestionnaire le password du gestionnaire du site
	 * 
	 * @throws MetierException           si le <code>passwordGestionnaire</code> est
	 *                                   invalide, si le
	 *                                   <code>passwordGestionnaire</code> est
	 *                                   incorrect.
	 * @throws JoueurInexistantException levée si il n'y a pas de joueur avec le
	 *                                   même <code>nom</code>, <code>prenom</code>
	 *                                   et <code>pseudo</code>.
	 * @throws JoueurException           levée si le joueur a des paris en cours, si
	 *                                   <code>nom</code>, <code>prenom</code>,
	 *                                   <code>pseudo</code> sont invalides.
	 * 
	 * @return le nombre de jetons à rembourser au joueur qui vient d'être
	 *         désinscrit.
	 * 
	 */
	public long desinscrireJoueur(String nom, String prenom, String pseudo, String passwordGestionnaire)
			throws MetierException, JoueurInexistantException, JoueurException {
		long jetons = 0;
		Joueur joueur = new Joueur(nom, prenom, pseudo);
		
		validitePasswordGestionnaire(passwordGestionnaire);
		this.gestionnaire.rightPassword(passwordGestionnaire);//MetierException
		validiteJoueurInexistant(joueur); 			//JoueurInexistantException
		validiteJoueurRetire(joueur); 				//JoueurInexistantException
		
		
		jetons = joueur.getSolde();
		joueursRetire.add(joueur);
		joueurs.remove(joueur);
		return jetons;
	}

	/**
	 * ajouter une compétition.
	 * 
	 * @param competition          le nom de la compétition
	 * @param dateCloture          la date à partir de laquelle il ne sera plus
	 *                             possible de miser
	 * @param competiteurs         les noms des différents compétiteurs de la
	 *                             compétition
	 * @param passwordGestionnaire le password du gestionnaire du site
	 * 
	 * @throws MetierException               levée si le tableau des compétiteurs
	 *                                       n'est pas instancié, si le
	 *                                       <code>passwordGestionnaire</code> est
	 *                                       invalide, si le
	 *                                       <code>passwordGestionnaire</code> est
	 *                                       incorrect.
	 * @throws CompetitionExistanteException levée si une compétition existe avec le
	 *                                       même nom.
	 * @throws CompetitionException          levée si le nom de la compétition ou
	 *                                       des compétiteurs sont invalides, si il
	 *                                       y a moins de 2 compétiteurs, si un des
	 *                                       competiteurs n'est pas instancié, si
	 *                                       deux compétiteurs ont le même nom, si
	 *                                       la date de clôture n'est pas instanciée
	 *                                       ou est dépassée.
	 */
	public void ajouterCompetition(String competition, DateFrancaise dateCloture, String[] competiteurs,
			String passwordGestionnaire) throws MetierException, CompetitionExistanteException, CompetitionException {
		
		validitePasswordGestionnaire(passwordGestionnaire);    //MetierException
		this.gestionnaire.rightPassword(passwordGestionnaire); //MetierException
		validiteCompetitionExistant(competition);//CompetitionExistanteException
		validiteCompetition(competition);				  //CompetitionException
		validiteDate(dateCloture);						  //CompetitionException
		validiteCompetiteurs(competiteurs); 				   //MetierException
		if (dateCloture.estDansLePasse()) throw new CompetitionException();
		
		// pas d'erreur donc on ajoute la compétition
		competitions.add(new Competition(competition, dateCloture, competiteurs));
	}

	/**
	 * solder une compétition vainqueur (compétition avec vainqueur).
	 * 
	 * Chaque joueur ayant misé sur cette compétition en choisissant ce compétiteur
	 * est crédité d'un nombre de jetons égal à :
	 * 
	 * (le montant de sa mise * la somme des jetons misés pour cette compétition) /
	 * la somme des jetons misés sur ce compétiteur.
	 *
	 * Si aucun joueur n'a trouvé le bon compétiteur, des jetons sont crédités aux
	 * joueurs ayant misé sur cette compétition (conformément au montant de leurs
	 * mises). La compétition est "supprimée" si il ne reste plus de mises suite à
	 * ce solde.
	 * 
	 * @param competition          le nom de la compétition
	 * @param vainqueur            le nom du vainqueur de la compétition
	 * @param passwordGestionnaire le password du gestionnaire du site
	 * 
	 * @throws MetierException                 levée si le
	 *                                         <code>passwordGestionnaire</code> est
	 *                                         invalide, si le
	 *                                         <code>passwordGestionnaire</code> est
	 *                                         incorrect.
	 * @throws CompetitionInexistanteException levée si il n'existe pas de
	 *                                         compétition de même nom.
	 * @throws CompetitionException            levée si le nom de la compétition ou
	 *                                         du vainqueur est invalide, si il
	 *                                         n'existe pas de compétiteur du nom du
	 *                                         vainqueur dans la compétition, si la
	 *                                         date de clôture de la compétition est
	 *                                         dans le futur.
	 * 
	 */
	public void solderVainqueur(String competition, String vainqueur, String passwordGestionnaire)
			throws MetierException, CompetitionInexistanteException, CompetitionException {

		validitePasswordGestionnaire(passwordGestionnaire);   //MetierException
		this.gestionnaire.rightPassword(passwordGestionnaire);//MetierException
		validiteCompetitionInexistant(competition);//CompetitionInexistanteException
		validiteCompetitionTerminee(competition); 		//CompetitionException
		validiteVainqueur(competition, vainqueur); 		//CompetitionException
		
		//retourne la quantité de jetons misée sur le vainqueur 
		long sommeVainqueur = getSommeVainqueur(vainqueur, competition); 
		// retourne la somme totale des jetons misée sur les compétiteurs de la compétition
		long sommeCompetition = getCompetition(competition).getJetonsMise();
		//Mis à jour le solde attribué à chaque joueur
		misaJourSolde(competition, vainqueur, sommeVainqueur, sommeCompetition);

		// on gère l'historique des compétitions déjà soldées 
		competitionsSolde.add(getCompetition(competition));
		competitions.remove(getCompetition(competition));
	}

	/**
	 * 
	 * 	Mis à jour le solde attribué à chaque joueur lors d'un pari dans différents cas 
	 * 
	 * 
	 * @param competition          le nom de la compétition
	 * @param vainqueur            le nom du vainqueur de la compétition
	 * @param sommeVainqueur 	   la quantité de jetons misée sur le vainqueur 
	 * @param sommeCompetition 	   la quantité de jetons misée pour cette compétition
	 * 
	 *
	 * 
	 */	
	public void misaJourSolde(String competition, String vainqueur, long sommeVainqueur, long sommeCompetition) {
		long solde = 0;
		LinkedList<Parier> parisJoueur = null;
		for (int i = 0; i < joueurs.size(); i++) {			
			parisJoueur = joueurs.get(i).getParies(); //par chaque joueur
			if (parisJoueur.size() > 0) {				
				for (int numParis = 0; numParis < parisJoueur.size(); numParis++) { 			
					Parier paris = parisJoueur.get(numParis); //par chaque pari						
					if ((paris.getNomCompetition().equals(competition))	&& (paris.getVainqueurEnvisage().equals(vainqueur)) && sommeVainqueur > 0) {
						// si un joueur a parié sur le bon compétiteur, 
						// alors il se voit créditer d'un nombre de jetons égal à 'solde'
						solde = (paris.getMiseEnJetons() * sommeCompetition) / sommeVainqueur;
						joueurs.get(i).crediterSolde(solde);
						parisJoueur.get(numParis).setSolde(true);
					} else if (paris.getNomCompetition().equals(competition) && sommeVainqueur == 0) {
						// si aucun joueur n'a trouvé le bon compétiteur, 
						// chacun se voit créditer du nombre de jeton égal à sa mise
						solde = paris.getMiseEnJetons();
						joueurs.get(i).crediterSolde(solde);
						parisJoueur.get(numParis).setSolde(true);
					}else if (paris.getNomCompetition().equals(competition)){
						// le joueur n'a pas trouvé le vainqueur donc perd sa mise
						parisJoueur.get(numParis).setSolde(true);
					}

				}
			}
		}
	}

	/**
	 * créditer le compte en jetons d'un joueur du site de paris.
	 * 
	 * @param nom                  le nom du joueur
	 * @param prenom               le prénom du joueur
	 * @param pseudo               le pseudo du joueur
	 * @param sommeEnJetons        la somme en jetons à créditer
	 * @param passwordGestionnaire le password du gestionnaire du site
	 * 
	 * @throws MetierException           levée si le
	 *                                   <code>passwordGestionnaire</code> est
	 *                                   invalide, si le
	 *                                   <code>passwordGestionnaire</code> est
	 *                                   incorrect,//throw MetierException si la
	 *                                   somme en jetons est négative.
	 * @throws JoueurException           levée si <code>nom</code>,
	 *                                   <code>prenom</code>, <code>pseudo</code>
	 *                                   sont invalides.
	 * @throws JoueurInexistantException levée si il n'y a pas de joueur avec les
	 *                                   mêmes nom, prénom et pseudo.
	 */
	public void crediterJoueur(String nom, String prenom, String pseudo, long sommeEnJetons,
			String passwordGestionnaire) throws MetierException, JoueurException, JoueurInexistantException {
		
		validitePasswordGestionnaire(passwordGestionnaire); //MetierException
		this.gestionnaire.rightPassword(passwordGestionnaire);//MetierException
		validiteJoueur(nom, prenom, pseudo); 				  //JoueurException
		validiteJoueurInexistant(new Joueur(nom, prenom, pseudo));//JoueurInexistantException
		if (sommeEnJetons < 0) throw new MetierException(); //MetierException sommeEnJetons negative
		
		// pas d'erreur donc on crédite le joueur 
		getJoueur(nom, prenom, pseudo).crediterSolde(sommeEnJetons);

	}

	/**
	 * débiter le compte en jetons d'un joueur du site de paris.
	 * 
	 * @param nom                  le nom du joueur
	 * @param prenom               le prénom du joueur
	 * @param pseudo               le pseudo du joueur
	 * @param sommeEnJetons        la somme en jetons à débiter
	 * @param passwordGestionnaire le password du gestionnaire du site
	 * 
	 * @throws MetierException           levée si le
	 *                                   <code>passwordGestionnaire</code> est
	 *                                   invalide, si le
	 *                                   <code>passwordGestionnaire</code>
	 *                                   est incorrect, si la somme en jetons est
	 *                                   négative.
	 * @throws JoueurException           levée si <code>nom</code>,
	 *                                   <code>prenom</code>, <code>pseudo</code>
	 *                                   sont invalides si le compte en jetons du
	 *                                   joueur est insuffisant (il deviendrait
	 *                                   négatif).
	 * @throws JoueurInexistantException levée si il n'y a pas de joueur avec les
	 *                                   //throw MetierExceptionmêmes nom, prénom et
	 *                                   pseudo.
	 * 
	 */

	public void debiterJoueur(String nom, String prenom, String pseudo, long sommeEnJetons, String passwordGestionnaire)
			throws MetierException, JoueurInexistantException, JoueurException {
		
		validitePasswordGestionnaire(passwordGestionnaire); //MetierException
		this.gestionnaire.rightPassword(passwordGestionnaire); //MetierException 
		if (sommeEnJetons < 0) throw new MetierException(); //MetierException 
		validiteJoueur(nom, prenom, pseudo); 				//JoueurException
		validiteJoueurInexistant(new Joueur(nom, prenom, pseudo)); //JoueurInexistantException
		
		Joueur joueur = getJoueur(nom, prenom, pseudo);		
		if (joueur.getSolde() < sommeEnJetons)	throw new JoueurException();
		
		// pas d'erreur donc on débite le joueur 
		joueur.debiterSolde(sommeEnJetons);

	}

	/**
	 * consulter les joueurs.
	 * 
	 * @param passwordGestionnaire le password du gestionnaire du site de paris
	 * 
	 * @throws MetierException levée si le <code>passwordGestionnaire</code> est
	 *                         invalide, si le <code>passwordGestionnaire</code> est
	 *                         incorrect.
	 * 
	 * @return une liste de liste dont les éléments (de type <code>String</code>)
	 *         représentent un joueur avec dans l'ordre :
	 *         <ul>
	 *         <li>le nom du joueur</li>
	 *         <li>le prénom du joueur</li>
	 *         <li>le pseudo du joueur</li>
	 *         <li>son compte en jetons restant disponibles</li>
	 *         <li>le total de jetons engagés dans ses mises en cours.</li>
	 *         </ul>
	 */
	public LinkedList<LinkedList<String>> consulterJoueurs(String passwordGestionnaire) throws MetierException {
	
		validitePasswordGestionnaire(passwordGestionnaire);
		this.gestionnaire.rightPassword(passwordGestionnaire); //MetierException 
		
		LinkedList<LinkedList<String>> list = new LinkedList<LinkedList<String>>();

		for (int i = 0; i < joueurs.size(); i++) {
			LinkedList<String> data = new LinkedList<String>();
			Joueur joueur = joueurs.get(i);
						
			data.add(joueur.getNom());
			data.add(joueur.getPrenom());
			data.add(joueur.getPseudo());
			data.add(Long.toString(joueur.getSolde()));
			data.add(Long.toString(getJetonsEngages(joueur)));
			
			list.add(i, data);
		}
		return list;
	}


	/**
	 * miserVainqueur (parier sur une compétition, en désignant un vainqueur). Le
	 * compte du joueur est débité du nombre de jetons misés.
	 * 
	 * @param pseudo            le pseudo du joueur
	 * @param passwordJoueur    le password du joueur
	 * @param miseEnJetons      la somme en jetons à miser
	 * @param competition       le nom de la compétition relative au pari effectué
	 * @param vainqueurEnvisage le nom du compétiteur sur lequel le joueur mise
	 *                          comme étant le vainqueur de la compétition
	 * 
	 * @throws MetierException                 levée si la somme en jetons est
	 *                                         négative.
	 * @throws JoueurInexistantException       levée si il n'y a pas de joueur avec
	 *                                         les mêmes pseudos et password.
	 * @throws CompetitionInexistanteException levée si il n'existe pas de
	 *                                         compétition de même nom.
	 * @throws CompetitionException            levée si <code>competition</code> ou
	 *                                         <code>vainqueurEnvisage</code> sont
	 *                                         invalides, si il n'existe pas un
	 *                                         compétiteur de nom
	 *                                         <code>vainqueurEnvisage</code> dans
	 *                                         la compétition, si la compétition
	 *                                         n'est plus ouverte (la date de
	 *                                         clôture est dans le passé).
	 * @throws JoueurException                 levée si <code>pseudo</code> ou
	 *                                         <code>password</code> sont invalides,
	 *                                         si le <code>compteEnJetons</code> du
	 *                                         joueur est insuffisant (il
	 *                                         deviendrait négatif).
	 */
	public void miserVainqueur(String pseudo, String passwordJoueur, long miseEnJetons, String competition,
			String vainqueurEnvisage) throws MetierException, JoueurInexistantException,
			CompetitionInexistanteException, CompetitionException, JoueurException {
		
		
		if (miseEnJetons < 0) throw new MetierException();  //MetierException
		validiteJoueur(pseudo,passwordJoueur);				//JoueurException
		validiteJoueurInexistant(pseudo,passwordJoueur); //JoueurInexistantException	
		validiteCompetition(competition,vainqueurEnvisage);//CompetitionException
		validiteVainqueur(competition,vainqueurEnvisage);  //CompetitionException
		validiteCompetitionNoTerminee(competition);        //CompetitionException
		
		Joueur joueur = getJoueur(pseudo,passwordJoueur);
		if (joueur.getSolde() < miseEnJetons) throw new JoueurException(); //JoueurException
		
		//si pas d'erreur alors on prend en compte le pari du joueur et on débite son compte
		joueur.setParies(new Parier(competition, vainqueurEnvisage, miseEnJetons)); 
		// mise a jour des jetons sur la competition
		getCompetition(competition).setJetonsMise(miseEnJetons); 
		joueur.debiterSolde(miseEnJetons);
	}

	/**
	 * connaître les compétitions en cours.
	 * 
	 * @return une liste de liste dont les éléments (de type <code>String</code>)
	 *         représentent une compétition avec dans l'ordre :
	 *         <ul>
	 *         <li>le nom de la compétition,</li>
	 *         <li>la date de clôture de la compétition.</li>
	 *         </ul>
	 */
	public LinkedList<LinkedList<String>> consulterCompetitions() {
		LinkedList<LinkedList<String>> list = new LinkedList<LinkedList<String>>();

		for (int i = 0; i < competitions.size(); i++) {
			LinkedList<String> data = new LinkedList<String>();
			
			data.add(competitions.get(i).getNom());
			data.add(competitions.get(i).getDate().toString());

			list.add(i, data);
		}
		return list;

	}

	/**
	 * connaître la liste des noms des compétiteurs d'une compétition.
	 * 
	 * @param competition le nom de la compétition
	 * 
	 * @throws CompetitionException            levée si le nom de la compétition est
	 *                                         invalide.
	 * @throws CompetitionInexistanteException levée si il n'existe pas de
	 *                                         compétition de même nom.
	 * 
	 * @return la liste des compétiteurs de la compétition.
	 */
	public LinkedList<String> consulterCompetiteurs(String competition)
			throws CompetitionException, CompetitionInexistanteException {
		
		validiteCompetition(competition); //throw CompetitionException
		validiteCompetitionInexistant(competition); //throw CompetitionInexistant

		LinkedList<String> list = new LinkedList<String>();
		String[] competiteurs = getCompetition(competition).getCompetiteurs();
		
		for (int i = 0; i < competiteurs.length; i++) {
			list.add(competiteurs[i]);
		}
		return list;
	}

	//----- les méthodes d'utilités
	/**
	 * obtenir un joueur de la liste des joueurs avec le nom, 
	 * prenom et pseudo
	 * 
	 * @param nom			le nom du joueur
	 * @param prenom		le prenom du joueur
	 * @param pseudo		le pseudo du joueur
	 * 
	 * @return un object de la classe Joueur qui a le même nom, 
	 * 		   prenom et pseudo
	 *         Si ne le trouve pas, return <null> 
	 * 
	 */
	protected Joueur getJoueur(String nom, String prenom, String pseudo) {
		Joueur joueur = null;
		for (int i = 0; i < joueurs.size(); i++) {
			if (joueurs.get(i).isEquals(new Joueur(nom, prenom, pseudo))) {
				joueur = joueurs.get(i);
			}
		}
		return joueur;
	}
	
	/**
	 * obtenir un joueur de la liste des joueurs avec le pseudo 
	 * et password
	 * 
	 * @param pseudo			le pseudo du joueur
	 * @param passwordJoueur	le prenom du joueur
	 * 
	 * @return un objet de la classe Joueur avec le même pseudo, 
	 * 		   et password.
	 * 		   Si ne le trouve pas, return <null>  
	 *         
	 * 
	 */
	protected Joueur getJoueur(String pseudo, String passwordJoueur) {
		Joueur joueur = null;
		for (int i = 0; i < joueurs.size(); i++) {
			if (joueurs.get(i).isEquals(pseudo,passwordJoueur)) {
				joueur = joueurs.get(i);
			}
		}
		return joueur;
	}

	

	
	/**
	 * obtenir une compétition de la liste des compétitions avec le nom 
	 * de la compétition
	 * 
	 * @param nom			le nom de la compétition
	 * 
	 * @return un object de la classe Competition avec le même nom
	 * 		   Si ne le trouve pas, return <null>  
	 * 
	 */
	protected Competition getCompetition(String competition) {
		Competition c = null;
		for (int i = 0; i < competitions.size(); i++) {
			if (competitions.get(i).equals(competition)) {
				c = competitions.get(i);
			}
		}
		return c;
	}

	/**
	 * calcule la quantité de jetons misée sur le vainqueur
	 * 
	 * @param vainqueur		le nom du vainqueur de la compétition
	 * @param competition	le nom de la compétition 
	 * 
	 * @return le nombre total de jetons misée sur le vainqueur
	 * 
	 */
	public long getSommeVainqueur(String vainqueur, String competition) {
		long miseACompetition = 0;
		LinkedList<Parier> parisJoueur = null;

		for (int i = 0; i < joueurs.size(); i++) {
			parisJoueur = joueurs.get(i).getParies();
			if (parisJoueur.size() > 0) {
				for (int numParis = 0; numParis < parisJoueur.size(); numParis++) {
					Parier paris = parisJoueur.get(numParis);
					if ((paris.getNomCompetition().equals(competition))
							&& (paris.getVainqueurEnvisage().equals(vainqueur))) {
						// somme des jetons mises sur ce competiteur
						miseACompetition = miseACompetition + parisJoueur.get(numParis).getMiseEnJetons();
					}
				}
			}
		}
		return miseACompetition;
	}
	
	
	/**
	 * calcule la quantité de jetons qui est engagé
	 * 
	 * @param nom			le nom du joueur
	 * @param prenom		le prenom du joueur
	 * @param pseudo		le pseudo du joueur
	 * 
	 * @return un object de la classe Joueur avec le même nom, prenom et pseudo
	 *         
	 * 
	 */
	public long getJetonsEngages(Joueur joueur){		
		LinkedList <Parier> listParis = joueur.getParies();
		long solde = 0;
		
		for(int i = 0; i < listParis.size(); i++) {
			if(!listParis.get(i).isSolde()) {
				solde = solde + listParis.get(i).getMiseEnJetons();
			}
		}		
		return solde;
	}
	
	//---- Les méthodes du validite

	/**
	 * vérifier la validité du password du gestionnaire.
	 * 
	 * @param passwordGestionnaire le password du gestionnaire à vérifier
	 * 
	 * @throws MetierException levée si le <code>passwordGestionnaire</code> est
	 *                         invalide.
	 */ 
	protected void validitePasswordGestionnaire(String passwordGestionnaire) throws MetierException {
		if (passwordGestionnaire == null)
			throw new MetierException();
		if (!passwordGestionnaire.matches("[0-9A-Za-z]{8,}"))
			throw new MetierException();
	}	
	
	/**
	 * vérifier la validité d'un joueur (le nom, le prenom et le pseudo sont valides)
	 * 
	 * @param nom                  le nom du joueur
	 * @param prenom               le prénom du joueur
	 * @param pseudo               le pseudo du joueur
	 * 
	 * @throws JoueurException         levée si <code>nom</code>,
	 *                                 <code>prenom</code>, <code>pseudo</code> sont
	 *                                 invalides.
	 * 
	 */ 
	protected void validiteJoueur(String nom, String prenom, String pseudo) throws JoueurException {
		if (nom == null || prenom == null || pseudo == null)
			throw new JoueurException();
		if (!nom.matches("[A-Za-z]{1,}"))
			throw new JoueurException();
		if (!prenom.matches("[A-Za-z]{1,}"))
			throw new JoueurException();
		if (!pseudo.matches("[0-9A-Za-z]{4,}"))
			throw new JoueurException();
	}
	
	/**
	 * vérifier la validité d'un joueur (le password et le pseudo sont valides)
	 * 
	 * @param pseudo               le pseudo du joueur
	 * @param password             le password du joueur 
	 * 
	 * @throws JoueurException         levée si <code>pseudo</code>,
	 *                                 <code>password</code>, sont
	 *                                 invalides.
	 * 
	 */ 
	protected void validiteJoueur(String pseudo,String password) throws JoueurException {
		if (pseudo == null || password == null)
			throw new JoueurException();
		if (!password.matches("[0-9A-Za-z]{8,}"))
			throw new JoueurException();
		if (!pseudo.matches("[0-9A-Za-z]{4,}"))
			throw new JoueurException();
	}

	/**
	 * vérifie l'existence d'un joueur 
	 * 
	 * @param joueur               le joueur : instance de la classe Joueur
	 * 
	 * @throws JoueurExistantException         levée si un <code>joueur</code> existe 
	 * 										   avec les mêmes nom, prénom ou pseudo
	 * 
	 */  
	protected void validiteJoueurExistant(Joueur joueur) throws JoueurExistantException {
		for (int i = 0; i < joueurs.size(); i++) {
			joueurs.get(i).equals(joueur);
		}
	}
	
	/**
	 * vérifie si un joueur existe 
	 * 
	 * @param joueur               le joueur : instance de la classe Joueur
	 * 
	 * @throws JoueurInexistantException         levée s'il n'y a pas de <code>joueur</code> 
	 * 											 avec le même nom, prénom et pseudo 
	 * 
	 */ 
	protected void validiteJoueurInexistant(Joueur joueur) throws JoueurInexistantException {
		int count = 0;
		for (int i = 0; i < joueurs.size(); i++) {
			if (joueurs.get(i).isEquals(joueur))
				count++;
		}
		if (count == 0) throw new JoueurInexistantException();
	}
	
	/**
	 * vérifier si un joueur s'est déjà désinscrit
	 * 
	 * @param joueur               le joueur : instance de la classe Joueur
	 * 
	 * @throws JoueurInexistantException         levée si le <code>joueur</code> est déjà désinscrit 
	 * 
	 */ 
	protected void validiteJoueurRetire(Joueur joueur) throws JoueurInexistantException {
		for (int i = 0; i < joueursRetire.size(); i++) {
			if (joueursRetire.get(i).isEquals(joueur))
				throw new JoueurInexistantException();
		}
	}
	
	/**
	 * vérifier la validité d'une compétition (avec le nom de la compétition)
	 * 
	 * @param competition			le nom de la compétition
	 * 
	 * @throws CompetitionException         levée si <code>competition</code> est
	 *                                      invalide.
	 * 
	 */ 
	protected void validiteCompetition(String competition) throws CompetitionException {
		if (competition == null)
			throw new CompetitionException();
		if (!competition.matches("[A-Za-z0-9-_.]{4,}"))
			throw new CompetitionException();

	}
	
	/**
	 * vérifier la validité d'une compétition (avec le nom de la compétition et un compétiteur)
	 * 
	 * @param competition			le nom de la compétition
	 * @param competiteur			le nom du compétiteur
	 * 
	 * @throws CompetitionException         levée si <code>competition</code> et <code>competiteur</ code>
	 * 									    sont invalides.
	 * 
	 */ 
	protected void validiteCompetition(String competition,String competiteur) throws CompetitionException {
		validiteCompetition(competition);
		if (competiteur == null) throw new CompetitionException();
		if(!competiteur.matches("[A-Za-z0-9-_]{4,}")) throw new CompetitionException();
	}
	
	
	/**
	 * vérifier l'existence d'une compétition
	 * 
	 * @param competition			le nom de la compétition 
	 * 
	 * @throws CompetitionExistanteException       levée si une <code>competition</code> existe 
	 * 										   	   avec le même nom
	 * 
	 */ 
	protected void validiteCompetitionExistant(String competition) throws CompetitionExistanteException {
		if (getCompetition(competition) != null) throw new CompetitionExistanteException();
	}
	
	/**
	 * vérifier l'existence d'une compétition
	 * 
	 * @param competition			le nom de la compétition 
	 * 
	 * @throws CompetitionInexistanteException         levée s'il n'existe pas de compétition de même nom
	 * 
	 */ 
	protected void validiteCompetitionInexistant(String competition) throws CompetitionInexistanteException {		
		if (getCompetition(competition) == null) throw new CompetitionInexistanteException();
	}
	
	/**
	 * vérifier la validité d'une compétition (compétition terminée)
	 * 
	 * @param competition			le nom de la compétition 
	 * 
	 * @throws CompetitionException         levée si <code>competition</code> 
	 * 										a déjà fini
	 * 
	 */ 
	protected void validiteCompetitionTerminee(String competition) throws CompetitionException {
		Competition comp = getCompetition(competition);
		if (!comp.getDate().estDansLePasse()) throw new CompetitionException();
	}
	
	/**
	 * vérifier la validité d'une compétition (compétition pas terminée)
	 * 
	 * @param competition			le nom de la compétition 
	 * 
	 * @throws CompetitionException         levée si <code>competition</code> 
	 * 										n'a pas encore fini
	 * 
	 */ 
	protected void validiteCompetitionNoTerminee(String competition) throws CompetitionException {
		Competition comp = getCompetition(competition);
		if (comp.getDate().estDansLePasse()) throw new CompetitionException();		
	}
	
	/**
	 * vérifier l'existence d'un vainqueur dans une compétition donnée
	 * 
	 * @param competiteur			le nom du vainqueur
	 * @param competition			le nom de la compétition 
	 * 
	 * @throws CompetitionException         levée si n'existe pas
	 * 										le <code>competiteur</code> 
	 * 										vainqueur dans la compétition
	 * 
	 */ 
	protected void validiteVainqueur(String competition, String competiteur) throws CompetitionException {
		
		Competition comp = getCompetition(competition);		
		String[] listCompetiteurs = comp.getCompetiteurs();
		
		int counter = 0;
		for (int i = 0; i < listCompetiteurs.length; i++){
			if (listCompetiteurs[i].equals(competiteur))
				counter++;
		}
		if (counter == 0) throw new CompetitionException(); // Vainqueur inexistant

	}
	
	/**
	 * vérifier la validité de la date de cloture
	 * 
	 * @param dateCloture			la date de cloture de la compétition
	 * 
	 * @throws CompetitionException         levée si <code>dateCloture</code> 
	 * 										est null
	 * 
	 */ 
	protected void validiteDate(DateFrancaise dateCloture) throws CompetitionException {
		if (dateCloture == null)
			throw new CompetitionException();
	}
	
	/**
	 * vérifier la validité de la liste des competiteurs
	 * 
	 * @param competiteurs			la liste des competiteurs
	 * 
	 * @throws  MetierException         	levée si <code>competiteurs</code> 
	 * 										est vide
	 * @throws CompetitionException         levée si il n'y a pas au moins deux 
	 * 										competiteurs
	 * @throws CompetitionException         levée si il y a deux competiteurs
	 * 										avec le même nom
	 * 
	 */ 
	protected void validiteCompetiteurs(String[] competiteurs) throws MetierException, CompetitionException {
		
		//vérifier la validité des compétiteurs
		if (competiteurs == null) throw new MetierException();
		if (competiteurs.length < 2) throw new CompetitionException();	
				
		for (int i = 0; i < competiteurs.length; i++) {
			//vérifier la validité d'un compétiteur
			validiteCompetiteur(competiteurs[i]);			
			for (int j = 0; j < competiteurs.length; j++) {
				if (i != j) {
					if (competiteurs[i].equals(competiteurs[j]))
						throw new CompetitionException();
				}
			}
		}

		
	}
	
	/**
	 * vérifier la validité d'un competiteur
	 * 
	 * @param competiteur			le nom du competiteur
	 * 
	 * @throws CompetitionException         levée si <code>competiteur</code> 
	 * 										est invalide
	 * 
	 */ 
	protected void validiteCompetiteur(String competiteur) throws CompetitionException {
		if (competiteur == null) throw new CompetitionException(); 
		if (!competiteur.matches("[A-Za-z0-9-_]{4,}")) throw new CompetitionException();
	}
	
	/**
	 * vérifie l'inexistence d'un joueur avec le pseudo et le password
	 * 
	 * @param pseudo			le pseudo du joueur
	 * @param passwordJoueur	le password du joueur
	 * 
	 * @throws JoueurInexistantException    levée si n'y a pas un joueur
	 * 										aven le même nom et le 
	 * 										même password
	 */
	protected void validiteJoueurInexistant(String pseudo, String passwordJoueur) throws JoueurInexistantException {
		if (getJoueur(pseudo, passwordJoueur) == null)	throw new JoueurInexistantException();
	}	

	

}
