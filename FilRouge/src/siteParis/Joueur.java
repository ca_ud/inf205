package siteParis;

import java.util.LinkedList;

public class Joueur {
	//attributs de la classe
	private String nom;
	private String prenom;
	private String pseudo;
	private String password;
	private long solde;
	private LinkedList<Parier> paries;
	
	//constructeur de la classe 
	public Joueur(String nom, String prenom, String pseudo) {
		this.nom = nom;
		this.prenom = prenom;
		this.pseudo = pseudo;
		this.solde = 0;
		this.password = "";
		this.paries = new LinkedList<Parier>();
	}
	
	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	public LinkedList<Parier> getParies() {
		return paries;
	}

	public void setParies(Parier paries) {
		this.paries.add(paries);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public long getSolde() {
		return solde;
	}

	public void crediterSolde(long solde) {
		this.solde = this.solde + solde;
	}
	
	public void debiterSolde(long solde) {
		this.solde = this.solde - solde;
	}
	
    public void equals(Joueur autreJoueur) throws JoueurExistantException {
    	if(isEquals(autreJoueur)) {
    	  throw new JoueurExistantException();
    	}	
    	if(this.nom.equals(autreJoueur.nom) && this.prenom.equals(autreJoueur.prenom)) {
      	  throw new JoueurExistantException();
      	}
    	if(this.pseudo.equals(autreJoueur.pseudo)) {
      	  throw new JoueurExistantException();
      	}
    }
    
    public boolean isEquals(Joueur autreJoueur){
    	return (this.nom.equals(autreJoueur.nom) && this.prenom.equals(autreJoueur.prenom) && this.pseudo.equals(autreJoueur.pseudo));
    }
    
    public boolean isEquals(String pseudo, String passwordJoueur){
    	return (this.pseudo.equals(pseudo) && this.password.equals(passwordJoueur));
    }
    
	public String generatePassword() {
		String NUMEROS = "0123456789";
		String MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";
		
		int length = 8;
		String key = NUMEROS + MAYUSCULAS + MINUSCULAS;		
		String pswd = "";
 
		for (int i = 0; i < length; i++) {
			pswd+=(key.charAt((int)(Math.random() * key.length())));
		}
 
		return pswd;
	}
    
    
}
