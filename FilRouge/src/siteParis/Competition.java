package siteParis;

import java.util.Date;
import java.util.List;
import java.util.LinkedList;

public class Competition {
	//attributs de la classe
    private String[] competiteurs;
    private long jetonsMise; 
    private String nom;
    private DateFrancaise date;
    
    //constructeur de la classe
    public Competition(String competition,DateFrancaise dateCloture,String[] competiteurs){
		this.nom = competition;
		this.date = dateCloture;
		this.competiteurs = competiteurs;
		this.jetonsMise = 0;
	}
    
	public String[] getCompetiteurs() {
		return competiteurs;
	}

	public void setCompetiteurs(String[] competiteurs) {
		this.competiteurs = competiteurs;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public DateFrancaise getDate() {
		return date;
	}

	/**
	 * Setter of the property <tt>date</tt>
	 * @param date  The date to set.
	 * @uml.property  name="date"
	 */
	public void setDate(DateFrancaise date) {
		this.date = date;
	}

	public long getJetonsMise() {
		return jetonsMise;
	}

	public void setJetonsMise(long jetonsMise) {
		this.jetonsMise = this.jetonsMise + jetonsMise;
	}
	
	public boolean equals(String competition) {
		return this.nom.equals(competition);
	}
	
}
