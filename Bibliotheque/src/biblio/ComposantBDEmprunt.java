package biblio;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Composant logiciel assurant la gestion des emprunts d'exemplaires
 * de livre par les abonnés.
 */
public class ComposantBDEmprunt {

  /**
   * Retourne le nombre total d'emprunts en cours référencés dans la base.
   * 
   * @return le nombre d'emprunts.
   * @throws SQLException en cas d'erreur de connexion à la base.
   */
  public static int nbEmpruntsEnCours() throws SQLException {
	int counter  = 0;
	
	Statement stmt = Connexion.getConnection().createStatement();
	String query = "SELECT COUNT(ID_EXEMPLAIRE) AS COUNTER FROM EMPRUNTE WHERE DATE_RETOUR IS NULL";
	
	ResultSet rset = stmt.executeQuery(query);
	
	rset.next();
	counter = rset.getInt("COUNTER");
	
	rset.close();
	stmt.close();

    return counter;
  }

  /**
   * Retourne le nombre d'emprunts en cours pour un abonné donné connu
   * par son identifiant.
   * 
   * @return le nombre d'emprunts.
   * @throws SQLException en cas d'erreur de connexion à la base.
   */
  public static int nbEmpruntsEnCours(int idAbonne) throws SQLException {
	  int counter  = 0;
		
		Statement stmt = Connexion.getConnection().createStatement();
		String query = "SELECT COUNT(ID_EXEMPLAIRE) AS COUNTER FROM EMPRUNTE WHERE ID_USAGER = " + idAbonne + "AND DATE_RETOUR IS NULL ";
		
		ResultSet rset = stmt.executeQuery(query);
		
		rset.next();
		counter = rset.getInt("COUNTER");
		
		rset.close();
		stmt.close();
    return counter;
  }

  
  /**
   * Récupération de la liste complète des emprunts en cours.
   * 
   * @return un <code>ArrayList<String[]></code>. Chaque tableau de chaînes
   * de caractères contenu correspond à un emprunt en cours.<br/>
   * Il doit contenir 8 éléments (dans cet ordre) :
   * <ul>
   *   <li>0 : id de l'exemplaire</li>
   *   <li>1 : id du livre correspondant</li>
   *   <li>2 : titre du livre</li>
   *   <li>3 : son auteur</li>
   *   <li>4 : id de l'abonné</li>
   *   <li>5 : nom de l'abonné</li>
   *   <li>6 : son prénom</li>
   *   <li>7 : la date de l'emprunt</li>
   * </ul>
   * @throws SQLException en cas d'erreur de connexion à la base.
   */
  public static ArrayList<String[]> listeEmpruntsEnCours() throws SQLException {
    ArrayList<String[]> emprunts = new ArrayList<String[]>();
    
    Statement stmt = Connexion.getConnection().createStatement();
    String query = "SELECT * FROM EMPRUNTE INNER JOIN EXEMPLAIRE ON EMPRUNTE.ID_EXEMPLAIRE = EXEMPLAIRE.ID INNER JOIN USAGER ON EMPRUNTE.ID_USAGER = USAGER.ID INNER JOIN LIVRE ON EXEMPLAIRE.ID_LIVRE = LIVRE.ID  WHERE EMPRUNTE.DATE_RETOUR IS NULL";
    
    ResultSet rset = stmt.executeQuery(query);
    
    while(rset.next())
    {
    	String[] arrayEmpruntsEnCours = new String[8];
    	arrayEmpruntsEnCours[0] = rset.getString("id_exemplaire");
    	arrayEmpruntsEnCours[1] = rset.getString("id_livre");
    	arrayEmpruntsEnCours[2] = rset.getString("titre");
    	arrayEmpruntsEnCours[3] = rset.getString("auteur");
    	arrayEmpruntsEnCours[4] = rset.getString("id_usager");
    	arrayEmpruntsEnCours[5] = rset.getString("nom");
    	arrayEmpruntsEnCours[6] = rset.getString("prenom");
    	arrayEmpruntsEnCours[7] = rset.getString("date_emprunte");
    	emprunts.add(arrayEmpruntsEnCours);
    }
    
    rset.close();
    stmt.close();
    
    return emprunts;
  }

  /**
   * Récupération de la liste des emprunts en cours pour un abonné donné.
   * 
   * @return un <code>ArrayList<String[]></code>. Chaque tableau de chaînes
   * de caractères contenu correspond à un emprunt en cours pour l'abonné.<br/>
   * Il doit contenir 5 éléments (dans cet ordre) :
   * <ul>
   *   <li>0 : id de l'exemplaire</li>
   *   <li>1 : id du livre correspondant</li>
   *   <li>2 : titre du livre</li>
   *   <li>3 : son auteur</li>
   *   <li>4 : la date de l'emprunt</li>
   * </ul>
   * @throws SQLException en cas d'erreur de connexion à la base.
   */
  public static ArrayList<String[]> listeEmpruntsEnCours(int idAbonne) throws SQLException {
    ArrayList<String[]> emprunts = new ArrayList<String[]>();
    
    Statement stmt = Connexion.getConnection().createStatement();
    String query = "SELECT * FROM EMPRUNTE INNER JOIN EXEMPLAIRE ON EMPRUNTE.ID_EXEMPLAIRE = EXEMPLAIRE.ID INNER JOIN LIVRE ON EXEMPLAIRE.ID_LIVRE = LIVRE.ID  WHERE EMPRUNTE.DATE_RETOUR IS NULL";

    
    ResultSet rset = stmt.executeQuery(query);
    
    while(rset.next())
    {
    	String[] arrayEmpruntsEnCours = new String[5];
    	arrayEmpruntsEnCours[0] = rset.getString("id_exemplaire");
    	arrayEmpruntsEnCours[1] = rset.getString("id_livre");
    	arrayEmpruntsEnCours[2] = rset.getString("titre");
    	arrayEmpruntsEnCours[3] = rset.getString("auteur");
    	arrayEmpruntsEnCours[4] = rset.getString("date_emprunte");
    	emprunts.add(arrayEmpruntsEnCours);
    }
    
    rset.close();
    stmt.close();
    return emprunts;
  }

  /**
   * Récupération de la liste complète des emprunts passés.
   * 
   * @return un <code>ArrayList<String[]></code>. Chaque tableau de chaînes
   * de caractères contenu correspond à un emprunt passé.<br/>
   * Il doit contenir 9 éléments (dans cet ordre) :
   * <ul>
   *   <li>0 : id de l'exemplaire</li>
   *   <li>1 : id du livre correspondant</li>
   *   <li>2 : titre du livre</li>
   *   <li>3 : son auteur</li>
   *   <li>4 : id de l'abonné</li>
   *   <li>5 : nom de l'abonné</li>
   *   <li>6 : son prénom</li>
   *   <li>7 : la date de l'emprunt</li>
   *   <li>8 : la date de retour</li>
   * </ul>
   * @return un <code>ArrayList</code> contenant autant de tableaux de String (5 chaînes de caractères) que d'emprunts dans la base.
   * @throws SQLException en cas d'erreur de connexion à la base.
   */
  public static ArrayList<String[]> listeEmpruntsHistorique() throws SQLException {
    ArrayList<String[]> emprunts = new ArrayList<String[]>();
    
    Statement stmt = Connexion.getConnection().createStatement();
    String query = "SELECT * FROM EMPRUNTE " +
    		"INNER JOIN EXEMPLAIRE ON EMPRUNTE.ID_EXEMPLAIRE = EXEMPLAIRE.ID " +
    		"INNER JOIN USAGER ON EMPRUNTE.ID_USAGER = USAGER.ID " +
    		"INNER JOIN LIVRE ON EXEMPLAIRE.ID_LIVRE = LIVRE.ID " +
    		"WHERE EMPRUNTE.DATE_RETOUR IS NOT NULL";
    
    ResultSet rset = stmt.executeQuery(query);
    
    while(rset.next())
    {
    	String[] arrayEmpruntsEnCours = new String[9];
    	arrayEmpruntsEnCours[0] = rset.getString("id_exemplaire");
    	arrayEmpruntsEnCours[1] = rset.getString("id_livre");
    	arrayEmpruntsEnCours[2] = rset.getString("titre");
    	arrayEmpruntsEnCours[3] = rset.getString("auteur");
    	arrayEmpruntsEnCours[4] = rset.getString("id_usager");
    	arrayEmpruntsEnCours[5] = rset.getString("nom");
    	arrayEmpruntsEnCours[6] = rset.getString("prenom");
    	arrayEmpruntsEnCours[7] = rset.getString("date_emprunte");
    	arrayEmpruntsEnCours[8] = rset.getString("date_retour");
    	emprunts.add(arrayEmpruntsEnCours);
    }
    
    rset.close();
    stmt.close();
    return emprunts;
  }

  /**
   * Emprunter un exemplaire à partir de l'identifiant de l'abonné et de
   * l'identifiant de l'exemplaire.
   * 
   * @param idAbonne : id de l'abonné emprunteur.
   * @param idExemplaire id de l'exemplaire emprunté.
   * @throws SQLException en cas d'erreur de connexion à la base.
   */
  public static void emprunter(int idAbonne, int idExemplaire) throws SQLException {
	  
	  if(!estEmprunte(idExemplaire)) {
		  //--------+ exemplaire  nest pas emprunte
		  Statement stmt = Connexion.getConnection().createStatement();
		  String sql = "INSERT INTO EMPRUNTE (id_exemplaire, id_usager, date_emprunte,date_retour) " +
		  				"VALUES (" + idExemplaire + "," + idAbonne + ", Now(),null)";
		  
		  stmt.executeUpdate(sql);
		  
		  stmt.close();
	  }else{
		  throw new SQLException("L'exemplaire " + idExemplaire + " est déjà en cours d'emprunt !");
	  }
		  
	  
  }

  /**
   * Retourner un exemplaire à partir de son identifiant.
   * 
   * @param idExemplaire id de l'exemplaire à rendre.
   * @throws SQLException en cas d'erreur de connexion à la base.
   */
  public static void rendre(int idExemplaire) throws SQLException {
	  
	  if(estEmprunte(idExemplaire)){
	    Statement stmt = Connexion.getConnection().createStatement();
	    String sql = "UPDATE EMPRUNTE SET DATE_RETOUR = NOW() " +
	    			 "WHERE ID_EXEMPLAIRE = " + idExemplaire + "AND DATE_RETOUR IS NULL";
	    
	    stmt.executeUpdate(sql);
	    
	    stmt.close();
	  }
  }
  
  /**
   * Détermine si un exemplaire sonné connu par son identifiant est
   * actuellement emprunté.
   * 
   * @param idExemplaire
   * @return <code>true</code> si l'exemplaire est emprunté, <code>false</code> sinon
   * @throws SQLException en cas d'erreur de connexion à la base.
   */
  public static boolean estEmprunte(int idExemplaire) throws SQLException {
    boolean estEmprunte = false;
    int count = 0;
    Statement stmt = Connexion.getConnection().createStatement();
    String query = "SELECT COUNT(ID_EXEMPLAIRE) AS COUNTER " +
    			"FROM EMPRUNTE WHERE (ID_EXEMPLAIRE = " + idExemplaire + "and DATE_RETOUR IS NULL)" ;
    
    ResultSet rset = stmt.executeQuery(query);    
    rset.next();
    
    count = rset.getInt("COUNTER");    
    if(count != 0) estEmprunte = true; 
    
    rset.close();
    stmt.close();
    
    return estEmprunte;
  }

  /**
   * Récupération des statistiques sur les emprunts (nombre d'emprunts et de
   * retours par jour).
   * 
   * @return un <code>HashMap<String, int[]></code>. Chaque enregistrement de la
   * structure de données est identifiée par la date (la clé) exprimée sous la forme
   * d'une chaîne de caractères. La valeur est un tableau de 2 entiers qui représentent :
   * <ul>
   *   <li>0 : le nombre d'emprunts</li>
   *   <li>1 : le nombre de retours</li>
   * </ul>
   * Exemple :
   * <pre>
   * +-------------------------+
   * | "2017-04-01" --> [3, 1] |
   * | "2017-04-02" --> [0, 1] |
   * | "2017-04-07" --> [5, 9] |
   * +-------------------------+
   * </pre>
   *   
   * @throws SQLException
   */
  public static HashMap<String, int[]> statsEmprunts() throws SQLException
  {
    HashMap<String, int[]> stats = new HashMap<String, int[]>();
    
    Statement stmt = Connexion.getConnection().createStatement();
    String query =  "SELECT DISTINCT DATE_EMPRUNTE AS FECHA FROM EMPRUNTE UNION " +
    		" SELECT DISTINCT DATE_RETOUR FROM EMPRUNTE WHERE DATE_RETOUR IS NOT NULL" +
    		" ORDER BY FECHA;";
    ResultSet rset = stmt.executeQuery(query);
    
    while(rset.next())
    {
		int[] counters = new int[2];
		String date = rset.getString("FECHA"); // all dates from emprunts and retours		
		counters[0] = getHistorique("DATE_EMPRUNTE", date);  // return number of emprunts
		counters[1] = getHistorique("DATE_RETOUR", date); 		// return number of retours

		//add to hashmap
		stats.put(date, counters);
    }
    
    rset.close();
	stmt.close();
    return stats;
  
}

 protected static int getHistorique(String column, String date) throws SQLException{
	 int count = 0;
	 
	 Statement stmt = Connexion.getConnection().createStatement();
	 //Search and count by date
	 String query = "SELECT COUNT("+column+ ") AS COUNTER FROM EMPRUNTE WHERE "+ column + " = '"+date+"'"; 
	 
	 ResultSet rset = stmt.executeQuery(query);
	 rset.next();
	 
	 count = rset.getInt("COUNTER"); 
	 rset.close();
	 stmt.close();
	 
	 return count;
 }
}

