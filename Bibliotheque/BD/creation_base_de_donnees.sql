-- Titre             : Script SQL (PostgreSQL) de création de la base de données du projet bibliothèque
-- Version           : 1.0
-- Date création     : 07 mars 2006
-- Date modification : 9 avril 2017
-- Auteur            : Philippe TANGUY
-- Description       : Ce script est une ébauche, à compléter, qui permet la création de la table
--                     "livre" pour la réalisation de la fonctionnalité "liste de tous les livres".

-- +----------------------------------------------------------------------------------------------+
-- | Suppression des tables                                                                       |
-- +----------------------------------------------------------------------------------------------+

drop table if exists "emprunte";
drop table if exists "exemplaire";
drop table if exists "livre";
drop table if exists "usager";


-- +----------------------------------------------------------------------------------------------+
-- | Création des tables                                                                          |
-- +----------------------------------------------------------------------------------------------+

create table livre
(
	id     serial primary key,
	isbn10 varchar(25) unique,
	isbn13 varchar(25) unique,
	titre  varchar(50) not null,
	auteur varchar(30) not null
);

create table exemplaire
(
	id serial primary key,
	id_livre integer references livre(id) ON DELETE CASCADE	
);

create table usager
(
	id serial primary key,
	nom varchar(25) not null,
	prenom varchar(25) not null,
	status  varchar(25) not null,
	email varchar(50) not null
);
-- + -------ON DELETE CASCADE: FK
-- + if a record in the parent table is DELETED, then the corresponding records in the child table will automatically be DELETED
create table emprunte
(
	id_exemplaire integer references exemplaire(id) ON DELETE CASCADE,
	id_usager integer references usager(id) ON DELETE CASCADE,
	date_emprunte date not null,
	date_retour date,
	primary key (id_exemplaire,date_emprunte) 
	
);

-- +----------------------------------------------------------------------------------------------+
-- | Insertion de quelques données de pour les tests                                              |
-- +----------------------------------------------------------------------------------------------+

insert into livre values(nextval('livre_id_seq'), '2-84177-042-7', NULL,                'JDBC et JAVA',                            'George REESE');    -- id = 1
insert into livre values(nextval('livre_id_seq'), NULL,            '978-2-7440-7222-2', 'Sociologie des organisations',            'Michel FOUDRIAT'); -- id = 2
insert into livre values(nextval('livre_id_seq'), '2-212-11600-4', '978-2-212-11600-7', 'Le data warehouse',                       'Ralph KIMBALL');   -- id = 3
insert into livre values(nextval('livre_id_seq'), '2-7117-4811-1', NULL,                'Entrepots de données',                    'Ralph KIMBALL');   -- id = 4
insert into livre values(nextval('livre_id_seq'), '2012250564',    '978-2012250567',    'Oui-Oui et le nouveau taxi',              'Enid BLYTON');     -- id = 5
insert into livre values(nextval('livre_id_seq'), '2203001011',    '978-2203001015',    'Tintin au Congo',                         'HERGÉ');           -- id = 6
insert into livre values(nextval('livre_id_seq'), '2012011373',    '978-2012011373',    'Le Club des Cinq et le trésor de l''île', 'Enid BLYTON');     -- id = 7
